__all__ = ['Explorer']
__author__ = 'Srijan Sharma'

from xpresso.ai.core.commons.utils.constants import \
    EXPLORE_UNIVARIATE_FILENAME, \
    EXPLORER_OUTPUT_PATH, EXPLORE_MULTIVARIATE_FILENAME, \
    DEFAULT_PROBABILITY_BINS, EXPLORER_UNSTRUCTURED_FILENAME
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.exploration.explorer_factory import ExplorerFactory
from xpresso.ai.core.data.exploration.render_exploration import \
    RenderExploration
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidDatatypeException
from xpresso.ai.core.logging.xpr_log import XprLogger

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class Explorer:

    def __init__(self, dataset):
        self.dataset = dataset
        self.explorer = ExplorerFactory.get_explorer(dataset)

    def understand(self, verbose=True):
        """
        Understands and assigns the datatype to the attributes
        Args:
            verbose('bool'): If True then renders the output
        """
        self.explorer.understand(verbose)

    def explore_univariate(self, verbose=True, to_excel=False,
                           validity_threshold=None,
                           output_path=EXPLORER_OUTPUT_PATH,
                           file_name=EXPLORE_UNIVARIATE_FILENAME,
                           bins=DEFAULT_PROBABILITY_BINS):
        """
        Univariate analysis of all the attributes
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the multivariate excel file to be
                stored
            file_name(str): file name of excel file
            validity_threshold(int): percent value for garbage threshold
            bins(int): No. of buckets for bar graph, default 20
        """
        self.explorer.explore_univariate(verbose, to_excel,
                                         validity_threshold,
                                         output_path,
                                         file_name,
                                         bins)

    def explore_multivariate(self, verbose=True, to_excel=False,
                             output_path=EXPLORER_OUTPUT_PATH,
                             file_name=EXPLORE_MULTIVARIATE_FILENAME):
        """
        Multivariate analysis of all the attributes
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the multivariate excel file to be
                stored
            file_name(str): file name of excel file
        """

        self.explorer.explore_multivariate(verbose, to_excel, output_path,
                                           file_name)

    def explore_unstructured(self, verbose=True, to_excel=False,
                             output_path=EXPLORER_OUTPUT_PATH,
                             file_name=EXPLORER_UNSTRUCTURED_FILENAME):
        """
        Multivariate analysis of all the attributes
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the multivariate excel file to be
                stored
            file_name(str): file name of excel file
        """
        self.explorer.explore_unstructured(verbose, to_excel, output_path,
                                           file_name)
